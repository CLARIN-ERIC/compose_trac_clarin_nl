#!/bin/bash

main() {
    STOP=0
    START=0
    BACKUP=0
    RESTORE=0
    STATUS=0
    LOGS=0
    RESTORE_ARG=""
    HELP=0
    TAG=""
    REMOVE=0

    #
    # Process script arguments
    #
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        stop)
            STOP=1
            ;;
        start)
            START=1
            ;;
        restart)
            STOP=1
            START=1
            ;;
        status)
            STATUS=1
            ;;
        logs)
            LOGS=1
            ;;
        backup)
            BACKUP=1
            ;;
        restore)
            RESTORE=1
            RESTORE_ARG1=$2
            shift
            ;;
        remove)
            REMOVE=1
            ;;
        -h|--help)
            HELP=1
            ;;
        *)
            echo "Unkown option: $key"
            HELP=1
            ;;
    esac
    shift # past argument or value
    done

    #
    # Execute based on mode argument
    #
    if [ ${HELP} -eq 1 ]; then
        echo ""
        echo "control.sh [start|stop|restart] [-h]"
        echo ""
        echo "  start                 Start"
        echo "  stop                  Stop"
        echo "  restart               Restart"
        echo "  status                Check status of all services"
        echo "  logs                  Tail logs of all services"
        echo "  remove                Remove all containers, volumes and networks"
        echo ""
        echo "  backup                Create a backup"
        echo "  restore               Restore a backup"
        exho "  restore ls            List available backups for restore"
        echo ""
        echo "  -h, --help    Show help"
        echo ""
        exit 0
    else
        COMPOSE_OPTS=""
        COMPOSE_DIR="clarin"
        if [ $(readlink $0) ]; then
            PROJECT_DIR=$(dirname $(readlink $0))
        else
            PROJECT_DIR=$(dirname $BASH_SOURCE)
        fi
        COMPOSE_DIR=$PROJECT_DIR/$COMPOSE_DIR

        #Set variables for a trac or svn backup
        BACKUP_PATH="backups"
        BACKUP_NAME="trac"

        if [ ${STOP} -eq 1 ]; then
            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml down $COMPOSE_OPTS)
        fi

        if [ "${BACKUP}" -eq 1 ]; then
            echo "Creating backup"
            (cd $COMPOSE_DIR && docker-compose exec -T ${BACKUP_NAME} /backup.sh)
            if [ "$?" -ne 0 ]; then
                exit "$?"
            fi

            echo "Cleaning up old files"
            cleanup "${BACKUP_PATH}/trac/"  "trac_(.+).tar.gz" "20060102_150405"

            exit 0
        fi

        if [ "${RESTORE}" -eq 1 ]; then
            SUPERVISORCTL_PASSWORD="thepassword"

            #Ensure a backup or command is supplied, valid commands are: ls, latest
            if [ -z "${RESTORE_ARG1}" ]; then
                echo "The name of the backup to restore must be supplied."
                echo "Available backups:"
                ls "${BACKUP_PATH}/trac/"
                echo "Aborting now"
                exit 1
            fi

            #List the available backups
            if [ "${RESTORE_ARG1}" == "ls" ]; then
                echo "Available backups:"
                ls "${BACKUP_PATH}/trac/"
                exit 0
            fi

            #Find the latest backup and use that
            if [ "${RESTORE_ARG1}" == "latest" ]; then
                LATEST=$(cd ${BACKUP_PATH}/trac/ && ls -t | head -1)
                echo "Latest=${LATEST}"
                RESTORE_ARG1=${LATEST}
            fi

            echo "Restoring (arg=${RESTORE_ARG1})"
            echo "Stopping ${BACKUP_NAME}"
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} stop apache2)
            echo "Restoring backup"
            (cd  ${BACKUP_PATH}/trac/ && ln -s ${RESTORE_ARG1} restore)
            (cd $COMPOSE_DIR && docker-compose up --force-recreate "${BACKUP_NAME}-preparer")
            rm ${BACKUP_PATH}/trac/restore
            echo "Starting ${BACKUP_NAME}"
            (cd $COMPOSE_DIR && docker-compose exec ${BACKUP_NAME} supervisorctl -u sysops -p ${SUPERVISORCTL_PASSWORD} start apache2)
            echo "Restore finished"
        fi

        if [ ${START} -eq 1 ]; then
            if [ ! -d "${BACKUP_PATH}" ]; then
                echo "Creating ${BACKUP_PATH}"
                mkdir -p "${BACKUP_PATH}"
            fi

            #Copy template .env
            if [ ! -f ".env" ]; then
                echo "Copying .env-template to .env"
                (cd $COMPOSE_DIR && cp ".env-template" "../../.env")

                echo "Generating random passwords in .env"
                GENERATED_PG_PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')
                sed -i "s/{{GENERATE_PG_PASSWORD}}/${GENERATED_PG_PASSWORD}/g" ".env"
            fi

            #Symlink .env file
            if [ ! -e "${COMPOSE_DIR}/.env" ]; then
                (cd $COMPOSE_DIR && ln -s "../../.env" ".env")
            fi

            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml up -d)
        fi

        if [ ${STATUS} -eq 1 ]; then
             (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml ps)
        fi

        if [ ${LOGS} -eq 1 ]; then
            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml logs -f)
        fi

        if [ ${REMOVE} -eq 1 ]; then
            (cd $COMPOSE_DIR && docker-compose -f docker-compose.yml down -v)
        fi
    fi
}

cleanup() {
    PATH=$1
    REGEX=$2
    DATE_FORMAT=$3

    DAYS=14 #keep daily backups for this number of days
    WEEKS=8 #keep weekly backups for this number of weeks
    MONTHS=36 #keep monthly backups for this number of months (anything older gets removed)

    (cd "${PATH}" && /usr/bin/file-clean -p "${REGEX}" -t "${DATE_FORMAT}" -d "${DAYS}" -w "${WEEKS}" -m "${MONTHS}")
    if [ "$?" -ne 0 ]; then
        exit "$?"
    fi
}

main "$@"; exit